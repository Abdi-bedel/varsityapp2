﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VarsityApp.Repositories;
using VarsityApp.Api;
using System.Threading.Tasks;

namespace VarsityApp.Models
{
    public class TeamSelector
    {
        SportRepository sportRepository = new SportRepository();

        public RegisterPlayer futsalMen(RegisterPlayer player)
        {
            //Decide if it is a 'special player' i.e. keeper first and Do you logic depeding on number trial.no...
            //If special player do this
            if (player.SpecialPlayer == true)
            {
                //Add player to new team 
                player.TrialTeam = player.TrialTeam + 1;

                //Add One more to special playerss
                sportRepository.AddSpecialPlayer(player.Sport, player.SportInfo.TotalTeams+1);
            }
            else
            {
                //Otherwise do this following futsal strucutre: for everywhere 5 players assign to new team
                //check total number of players
                if (player.SportInfo.NumberOfPlayers % 5 != 0)
                {
                    //Assign To current team
                    player.TrialTeam = player.SportInfo.TotalTeams;
                }
                else
                {
                    player.TrialTeam = player.SportInfo.TotalTeams + 1;
                    player.SportInfo.TotalTeams = player.SportInfo.TotalTeams + 1;
                }
                //Add to total number of players
                sportRepository.AddToTotalPlayers(player.Sport, player.SportInfo.NumberOfPlayers + 1);
            }

            return player;
        }
    }
}