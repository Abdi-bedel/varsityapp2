﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VarsityApp.Models
{
    public class Trials
    {
        public int SportID { get; set; }
        public int Phase { get; set; }
        public int NumberOfPlayers { get; set; }
        public int SpecialPlayers { get; set; }
        public int TotalTeams { get; set; }
    }
}