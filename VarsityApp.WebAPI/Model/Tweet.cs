﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VarsityApp.Models
{
    public class Tweet
    {
        public int TweetID { get; set; }
        public int PlayerID { get; set; }
        public int FullName { get; set; }
        public int SportID { get; set; }
        public string TweetContent { get; set; }
        public DateTime DatePosted { get; set; }
    }
}