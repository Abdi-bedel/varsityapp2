﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VarsityApp.Models
{
    public class Sport
    {
        public string SportName { get; set; }
        public int VarsityTeams { get; set; }
        public int NumberOfPlayers { get; set; }
        public int TotalTeams { get; set; }
        public int SpecialPlayers { get; set; }
    }
}