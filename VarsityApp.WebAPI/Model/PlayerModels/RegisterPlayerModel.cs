﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VarsityApp.Models
{
    public class RegisterPlayer
    {
        public int PlayerID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int Telephone { get; set; }
        public int Year { get; set; }
        public string Sport { get; set; }        
        public int Phase { get; set; }
        public string Position { get; set; }
        public bool SpecialPlayer { get; set; }
        public int TrialTeam { get; set; }
        public Sport SportInfo { get; set; }

    }
}