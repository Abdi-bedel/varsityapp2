﻿using System.Collections.Generic;

namespace VarsityApp.Models
{
    public class TrialPlayers
    {
        public int Phase { get; set; }
        public List<Team> Teams { get; set; }
    }
    public class Team
    {
        public int Teams { get; set; }
    }
}