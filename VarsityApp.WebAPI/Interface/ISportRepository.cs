﻿using System.Threading.Tasks;
using VarsityApp.Models;

namespace VarsityApp.Interface
{
    interface ISportRepository
    {
        Sport GetSportById(int sportID);
        Sport GetSportByName(string sportName);
        Sport AddSpecialPlayer(string sportName, int playercount);
        Sport AddToTotalPlayers(string sportName, int playercount);
    }
}
