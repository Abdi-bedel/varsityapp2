﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VarsityApp.Models;

namespace VarsityApp.Interface
{
    interface IPlayerRepository
    {
        RegisterPlayer AddPlayer(RegisterPlayer player);
    }
}
