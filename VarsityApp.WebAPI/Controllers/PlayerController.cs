﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using VarsityApp.Models;
using VarsityApp.Repositories;
using VarstiyApp.Api;

namespace VarsityApp.Api
{
    [EnableCors("*", "*", "*")]
    public class PlayerController : ApiController
    {
        PlayerRepository playerRepository = new PlayerRepository();
        SportRepository sportRepository = new SportRepository();
        TrialController trialController = new TrialController();
        RegisterPlayer player;

        [HttpPost]
        [Route("api/addplayer")]
        public RegisterPlayer AddNewPlayer([FromBody] RegisterPlayer player)
        {            
            if (player != null)
            {
                //Send to teamcontroller where emails and text are sent
                trialController.TeamController(player);

                //Add to total number of players
                player.SportInfo.NumberOfPlayers = player.SportInfo.NumberOfPlayers+ 1;
            }
            return player;
        }
    }
}
