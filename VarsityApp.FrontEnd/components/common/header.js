﻿import React, { Component } from 'react'
import AppBar from 'material-ui/AppBar';
import Authorization from './authorization.js';
import {Link} from 'react-router-dom'; 
import {connect} from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import LoginButton from '../login/login.js'

const Player = ['player', 'staff', 'admin'];
const Staff = ['staff', 'admin'];
const Admin = ['admin'];

class Header extends React.Component { 
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        }
        this.openDrawer = this.openDrawer.bind(this)
        this.closeDrawer = this.closeDrawer.bind(this)
    }
    
    openDrawer() { this.setState ({ open: true })};
    closeDrawer() { this.setState ({open: false})};

    render() {
        return (
            <div>
            <AppBar
                title="SportBU"
                onLeftIconButtonTouchTap={this.openDrawer}	
                iconElementRight={<LoginButton />}
            />
            <Drawer
                docked={false}
                width={200}
                open={this.state.open}
                onRequestChange={(open) => this.setState({open})}
          >
            <Link to="/"><MenuItem onTouchTap={this.closeDrawer}>Home</MenuItem></Link>
            {this.props.pages.signup ? 
                <Link to="/signup"><MenuItem onTouchTap={this.closeDrawer}>Sign Up</MenuItem></Link> : ''
            }
            {this.props.pages.ratingplayer ? 
                <Link to="/ratePlayers"><MenuItem onTouchTap={this.closeDrawer}>Rate Players</MenuItem></Link> : ''
            }
          </Drawer>
          </div>
        );
    }
}

function mapStateToProps(state){
    return {
        pages: state.user.pages
    };
}

export default connect(mapStateToProps)(Header);
