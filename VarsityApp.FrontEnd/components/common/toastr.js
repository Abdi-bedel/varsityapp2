﻿import {React, Component} from "react";
import {ToastContainer,ToastMessage} from "../../lib";

var ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

class Message extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <ToastContainer ref={this.props.ref}
                toastMessageFactory={ToastMessageFactory}
                className="toast-top-right" 
            />
        );
    }
}

export default Message;
