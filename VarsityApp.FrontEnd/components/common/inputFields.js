﻿import React from "react";
import TextField from 'material-ui/TextField';

class input extends React.Component {
    render() {
        return (
            <TextField
                floatingLabelText={this.props.label}
                name={this.props.name}
                fullWidth={true}
                value={this.props.value}
                type={this.props.type}
                onChange={this.props.onChange}
                errorText={this.props.errorText}
            />
        );
    }
}

export default input;
