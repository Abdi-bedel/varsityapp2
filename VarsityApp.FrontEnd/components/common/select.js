﻿import React from "react";
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import * as F from '../../firebase.js'

const Sports = ['Futsal', 'Football', 'Hockey', 'Table Tennis']
const Role = ['President', 'Captain', 'Staff', 'Admin']
const Year = [1, 2, 3, 4, 5]

class Select extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: []
        }
    }

    componentWillMount() {
        const choice = this.props.Selection;

        if(choice === 'Sports'){
            this.setState({option: Sports}) ;
        } else if(choice === 'Role'){
            if (this.props.currentUser === 'Captain' || this.props.currentUser === 'President') {
                //Remove from array staff and admin
                const role = Role.splice(2,2);
                this.setState({option: Role}) ;
            } else if(this.props.currentUser === 'Staff') {
                //Remove admin
                const role = Role.splice(3,1);
                this.setState({option: Role}) ;
            } else {
                this.setState({option: Role}) ;
            }
        } else if(choice === 'Year'){
            this.setState({option: Year})
        } else if(choice === 'Positions'){
            F.database.ref('Utills/Positions/' + this.props.Sport).on('value', snapshot => {
                const listoptions = [];
                const value = snapshot.val();
                if (value) {
                    Object.keys(value).map(x => {
                        listoptions.push(value[x])
                    })
                    this.setState({option: listoptions})
                }
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.id === 'Positions'){
            //Import Positions depeding on the sport
            const sport = nextProps.Sport;
            F.database.ref('Utills/Positions/' + sport).on('value', snapshot => {
                const listoptions = [];
                const value = snapshot.val();
                if (value) {
                    Object.keys(value).map(x => {
                        listoptions.push(value[x])
                    })
                    this.setState({option: listoptions})
                }
            })
        }
    }

    render() {
        return (
            <SelectField 
                type='select' 
                label={this.props.id}
                name={this.props.id}
                id={this.props.id}
                className={this.props.id}
                disabled={this.props.disabled}
                value={this.props.value}
                floatingLabelText={this.props.id}
                fullWidth={true}
                onChange={this.props.onChange}
            >
                {!this.state.option ? '' : 
                <MenuItem value={null} primaryText="" /> &&
                this.state.option.map((option) =>
                        <MenuItem key={option} value={option} primaryText={option} />
                )}
            </SelectField>
        );
    }
}

export default Select;