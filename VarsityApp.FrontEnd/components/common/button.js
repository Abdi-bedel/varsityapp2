﻿import React from "react";
import RaisedButton from 'material-ui/RaisedButton';

class input extends React.Component {
    render() {
        return (
            <RaisedButton 
                label={this.props.label} 
                primary={true} 
                fullWidth={true} 
                onClick={this.props.onClick} 
            />
        );
    }
}

export default input;
