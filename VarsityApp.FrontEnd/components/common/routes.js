﻿import React, { Component } from 'react'
import { Route } from 'react-router-dom'; 
import Authorization from './authorization.js';
import HomePage from '../home/homePage.js';
import RatingPageForm from '../rating/ratingPageForm.js';
import RegistrationForm from '../registration/registrationPage.js';

const Player = Authorization(['', 'staff', 'admin']);
const Staff = Authorization(['staff', 'admin']);
const Admin = Authorization(['admin']);

class routes extends React.Component {
    render() {
        return ( 
            <div>
                 <Route exact path="/" component={HomePage} />                    
                 <Route path="/signup" component={Player(RegistrationForm)} />
                 <Route path="/ratePlayers" component={Player(RatingPageForm)} />
            </div>
        );
    }
}

export default routes;