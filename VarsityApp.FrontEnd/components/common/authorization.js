﻿import React, {Component} from 'react';
import * as firebase from '../../firebase.js';

const Authorization = (allowedRoles) => (WrappedComponent) =>  
    class WithAuthorization extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                user: {
                    FullName: 'Abdi',
                    Role: 'admin'
                }
            }
        }

        render() {
            if (allowedRoles.includes(this.state.user.Role)) {
                return <WrappedComponent {...this.props} />
            } else {
                //Should show message and redirect to home
                return <h1>Not Authorized</h1>
            }
        }
    }

export default Authorization;