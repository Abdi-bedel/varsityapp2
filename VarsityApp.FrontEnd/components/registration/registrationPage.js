﻿import React from "react";
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import update from 'immutability-helper';
import { Row } from 'react-materialize';
import {registerPlayer} from '../../actions/registerActions.js'
import MyInput from '../common/inputFields.js'
import MyButton from '../common/button.js';
import Select from './Regselect.js';
import * as F from '../../firebase.js';

class signupPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                FullName: '',
                Sport: '',
                Year: null,
                Phone: '',
                Position: '',
                Email: '',
                Phase: 1,
                SportInfo: {}
            },
            errors: {
                FullName:'',
                Email:'',
                Phone: '',
            }
        };
        this.onTextChange = this.onTextChange.bind(this)
        this.onPositionChange = this.onPositionChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.onSelectSportChange = this.onSelectSportChange.bind(this)
        this.onSelectYearChange = this.onSelectYearChange.bind(this)
        this.addAlert = this.addAlert.bind(this)
    }   
    
    onTextChange(e, index, value) {
        const newState = update(this.state, {form: {[e.target.name]: {$set: e.target.value }}})
        this.setState(newState);
    }

    onPositionChange(e, index, value) {
        const newState = update(this.state, {form: {Position: {$set: value }}})
        this.setState(newState);
        console.log(newState)
    }

    onSelectYearChange(e, index, value){
        const newState = update(this.state, {form: {Year: {$set: value }}})
        this.setState(newState);
    }

    onSelectSportChange(e, index, value){
        const newState = update(this.state, {form: {Sport: {$set: value }}})
        this.setState(newState);
    }

    addAlert() {
        this.refs.container.success(
          "Welcome welcome welcome!!",
          "You are now home my friend. Welcome home my friend.", {
              timeOut: 30000,
              extendedTimeOut: 10000
          });
        window.open("http://youtu.be/3SR75k7Oggg")
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.validateForm()) {
            return;
        }
        F.database.ref('TrialTeamInfo/'+this.state.form.Sport).on('value', snapshot => {
            const sport = snapshot.val();
            const newState = update(this.state, {form: {SportInfo: {$set: sport }}})
            this.setState(newState);            
        })
        this.props.registerPlayer(this.state.form);
        //this.addAlert();
    }

    validateForm(e){
        const validForm = true;
        this.setState({ 
            errors: {
                FullName: '',            
                Phone: '',
                Email:''
            }
        });
        
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;   
        if (this.state.form.FullName === '') {
            this.setState({ errors: { FullName: "Enter your Name please" }});
            return false;
        }
        if (this.state.form.Email === '') {
            this.setState({ errors: { Email: 'Enter a Email please' }});
            return false;
        } else  if (!re.test(this.state.form.Email)){
            this.setState({ errors: { Email: 'Enter a valid Email please' }});
            return false;
        }
        if (this.state.form.Phone === '') {
            this.setState({ errors: { Phone: 'Enter your number' }});
            return false;
        }
        return validForm;
    }

    render() {
        return (
            <div>
                <MyInput
                    label="Enter Name"
                    name="FullName"
                    type="text"
                    value={this.state.form.FullName}
                    onChange={this.onTextChange}
                    errorText={this.state.errors.FullName}
                />
                <MyInput
                    label="Email"
                    name="Email"
                    type="email"
                    value={this.state.form.Email}
                    onChange={this.onTextChange}
                    errorText={this.state.errors.Email}
                />
                <MyInput
                    label="Contact Number"
                    name="Phone"
                    type="number"
                    value={this.state.form.Phone}
                    onChange={this.onTextChange}
                    errorText={this.state.errors.Phone}
                />
                <Select 
                    onSelectYearChange={this.onSelectYearChange}
                    valueYear={this.state.form.Year}
                    onSelectSportChange={this.onSelectSportChange}
                    valueSport={this.props.user.loggedIn ? this.props.user.sport : this.state.form.Sport}
                    disabledSport={this.props.user.loggedIn ? true : false}
                    onPositionChange={this.onPositionChange}
                    valuePosition={this.state.form.Position}
                    Sport={this.state.form.Sport}
                />

                <MyButton 
                    label="Sign Up!"
                    onClick={this.onSubmit} 
                />
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        user: state.user.user,
        newPlayer: state.register
    };
}

function matchDispatchToProps(dispath){
    return bindActionCreators({registerPlayer: registerPlayer}, dispath)
}

export default connect(mapStateToProps, matchDispatchToProps)(signupPage);
