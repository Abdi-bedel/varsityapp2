﻿import React from "react";
import Select from '../common/select.js';
import MenuItem from 'material-ui/MenuItem';
import * as F from '../../firebase.js'

const Sports = ['Futsal', 'Football', 'Hockey', 'Table Tennis']
const Role = ['President', 'Captain', 'Staff']
const Year = [1, 2, 3, 4, 5]

class RegSelects extends React.Component {
    render() {
        return (
            <div>
                <Select 
                    id="Year"
                    onChange={this.props.onSelectYearChange}
                    Selection={"Year"}
                    value={this.props.valueYear}
                />
                <Select 
                    id="Sport"
                    onChange={this.props.onSelectSportChange}
                    Selection={"Sports"}
                    value={this.props.valueSport}
                    disabled={this.props.disabledSport}
                />
                <Select 
                    id="Positions"
                    onChange={this.props.onPositionChange}
                    Selection={"Positions"}
                    value={this.props.valuePosition}
                    Sport={this.props.Sport}
                />
            </div>
        );
    }
}

export default RegSelects;