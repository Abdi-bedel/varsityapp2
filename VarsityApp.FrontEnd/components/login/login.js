﻿import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import LoginForm from './loginForm.js';
import {connect} from 'react-redux';
import {grey50} from 'material-ui/styles/colors';
import {bindActionCreators} from 'redux'
import * as actions from '../../actions/loginActions.js';
import * as F from '../../firebase.js';
import update from 'immutability-helper';

const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;       

const customContentStyle = {
    width: '40%',
    maxWidth: 'none',
};

class LoginDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openDialog: false,
            Name: '',
            Email: '',
            Sport: '',
            Password: '',
            Role: ''
        };
        this.handleClose = this.handleClose.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.signOut = this.signOut.bind(this);
        this.createUser = this.createUser.bind(this);        
        this.handleChange = this.handleChange.bind(this);
        this.onSelectSportChange = this.onSelectSportChange.bind(this);
        this.onSelectRoleChange = this.onSelectRoleChange.bind(this);
    }

    handleChange(event, logged) { this.setState({logged: logged}); };
    handleOpen() { this.setState({ openDialog: true }); };
    handleClose() { this.setState({ openDialog: false }); };
    handleInput(e) { this.setState({ [e.target.name]: e.target.value }) };
    onSelectSportChange(e, index, value){
        const newState = update(this.state, {Sport: {$set: value }})
        this.setState(newState);
    }
    onSelectRoleChange(e, index, value){
        const newState = update(this.state, {Role: {$set: value }})
        this.setState(newState);
    }

    onSubmit() { 
        const Pass = this.state.Password;
        const Email = this.state.Email;

        if (Email === '') {
            this.setState({ errors: { Email: 'Enter a Email please' }});
            return;
        } else  if (!re.test(Email)){
            this.setState({ errors: { Email: 'Enter a valid Email please' }});
            return;
        }
        console.log(Email, Pass)

        this.props.LoginUser(Email, Pass) 
    };
    createUser() {
        const newUser = {
            Email: this.state.Email,
            Name: this.state.Name,
            Sport: this.state.Sport,
            Role: this.state.Role,
            Pass: this.state.Password
        }
        if (newUser.Email === '') {
            this.setState({ errors: { Email: 'Enter a Email please' }});
            return false;
        } else  if (!re.test(newUser.Email)){
            this.setState({ errors: { Email: 'Enter a valid Email please' }});
            return false;
        }
        if (newUser.Name === '') {
            this.setState({ errors: { FullName: "Enter your Name please" }});
            return false;
        }
        this.props.newUser(newUser) 
    };
    signOut() { this.props.logOut() };
   
    render() {
        return (
          <div>
            <FlatButton 
                label={this.props.user.loggedIn ? "Add User/Log Out" : "LogIn"}
                onTouchTap={this.handleOpen}
                style={{color: grey50}}
            />
        <Dialog
          modal={false}
          open={this.state.openDialog}
          onRequestClose={this.handleClose}
          contentStyle={customContentStyle}
        >
          <LoginForm 
           handleInput={this.handleInput}
           onSubmit={this.onSubmit}
           role={this.props.user.role}
           createUser={this.createUser}
           onToggle={this.handleChange}
           logged={this.state.logged}
           signOut={this.signOut}
           loggedIn={this.props.user.loggedIn}
           Email={this.state.Email}
           Password={this.state.Password}
           Name={this.state.Name}
           Sport={this.state.Sport}
           Role={this.state.Role}
           onSelectSportChange={this.onSelectSportChange}
           onSelectRoleChange={this.onSelectRoleChange}
           Sport={this.state.Sport}
           Role={this.state.Role}
          />
        </Dialog>    
      </div>
    );
}
}

function mapStateToProps(state){
    return {
    user: state.user.user
    };
}

function matchDispatchToProps(dispath){
    return bindActionCreators({
        LoginUser: actions.LoginUser,
        logOut: actions.logOut,
        newUser: actions.newUser}, dispath)
}

export default connect(mapStateToProps, matchDispatchToProps)(LoginDialog);