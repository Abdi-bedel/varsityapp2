﻿import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import MyTextField from '../common/inputFields.js';
import Toggle from 'material-ui/Toggle';
import Select from '../common/select.js';

const toggle = { marginBottom: 16 }

class LoginForm extends React.Component {
    render() {
        return (
            <div>
                {!this.props.loggedIn ? '' :
                    <Toggle
                        label="New User"
                        onToggle={this.props.onToggle}
                    />
                }
                {this.props.loggedIn && !this.props.logged ? '' :
                    <div>
                        <MyTextField
                            label="Email"
                            onChange={this.props.handleInput}
                            name="Email"
                            type="email"
                            value={this.props.Email}
                        />
                        <MyTextField
                            label="Password"
                            onChange={this.props.handleInput}
                            name="Password"
                            type="password"
                            value={this.props.Password}
                            errorText={this.props.error}
                        />
                    </div>
                }
                {this.props.logged ? 
                    <div>
                        <MyTextField
                            label="Name"
                            onChange={this.props.handleInput}
                            name="Name"
                            type="text"
                            value={this.props.Name}
                        />
                        <Select 
                            id="Role"
                            onChange={this.props.onSelectRoleChange}
                            value={this.props.Role}
                            Selection={"Role"}
                        />
                        <Select 
                            id="Sport"
                            onChange={this.props.onSelectSportChange}
                            value={this.props.Sport}
                            Selection={"Sports"}
                        />
                        <RaisedButton 
                            label="Create user" 
                            fullWidth={true} 
                            primary={true} 
                            onClick={this.props.createUser} 
                       /> 
                   </div>
                    : 
                    <RaisedButton 
                        label={this.props.loggedIn ? "LogOut" : "LogIn"} 
                        fullWidth={true} 
                        primary={true}
                        onClick={this.props.loggedIn ? this.props.signOut: this.props.onSubmit} 
                   />
                }
            </div>
        );
    }
}
export default LoginForm;