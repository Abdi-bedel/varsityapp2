﻿import React, { Component } from 'react'
import Header from './common/header.js';
import Routes from './common/routes.js';
import { BrowserRouter  as Router, Route} from 'react-router-dom'; 

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Header />
                    <Routes />
                </div>
            </Router>
        );
    }
}

export default App;