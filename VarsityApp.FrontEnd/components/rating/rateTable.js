﻿import ReactDataGrid from 'react-data-grid';
import React from 'react';
import { Editors, Formatters } from 'react-data-grid-addons';

const { AutoComplete: AutoCompleteEditor, DropDownEditor } = Editors;
const { DropDownFormatter } = Formatters;

const titles = [{ id: 0, title: 'Yes' }, { id: 1, title: 'No' } ];

const NextPhase = <AutoCompleteEditor options={titles} />;

class Example extends React.Component {
    constructor(props) {
        super(props);
        this.rowGetter = this.rowGetter.bind(this);

        this._columns = [
          { key: 'FullName', name: 'Name' },
          { key: 'Year', name: 'Year' }, 
          { key: 'Team', name: 'Team' }, 
          { key: 'Skill1', name: 'Skill 1', editable: true}, 
          { key: 'Skill2', name: 'Skill 2', editable: true},
          { key: 'Skill3', name: 'Skill 3', editable: true},
          { key: 'NextPhase', name: 'Next Stage', editor: NextPhase }
        ];
    }

    rowGetter(i) {
        return this.props.row[i];
    }

    render() {
        return  (
          <ReactDataGrid
            enableCellSelect={true}
            columns={this._columns}
            rowGetter={this.rowGetter}
            rowsCount={this.props.row.length}
            onGridRowsUpdated={this.props.handleGridRowsUpdated}
          />
        );
    }
}


export default Example