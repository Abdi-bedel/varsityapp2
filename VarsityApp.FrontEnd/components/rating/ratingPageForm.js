﻿import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PhaseOne from './ratingPage.js';
import {Step, Stepper, StepLabel} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import * as F from '../../firebase.js';
import {ratePlayers} from '../../actions/trialActions.js';
import RateTable from './rateTable.js';
import update from 'react-addons-update';

class RatingPageForm extends React.Component {
    constructor(props){
        super(props); 
        this.state = {
            finished: false,
            stepIndex: 0,
            Phase: 1,
            Teams: [],
            row: [],
            TotalTeams: 0,
            errors: {
                Select: ''
            },
            players: []
        };
        this.handleGridRowsUpdated = this.handleGridRowsUpdated.bind(this);
        this.handlePhaseChange = this.handlePhaseChange.bind(this);
        this.onChange = this.onChange.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillMount(){
        F.database.ref('TrialTeamInfo/Futsal/')
           .once('value').then(snapshot => {
               const totalteams = snapshot.val(); 
               this.setState({
                   TotalTeams: totalteams.TotalTeams
               })
           });
    }

    handlePhaseChange(e, index, values) { this.setState({Phase: values}) }
    onChange(event, index, values) { this.setState({Teams: values}); }

    handleNext() {
        if(this.state.stepIndex >= 1){ return; }
        if(this.state.Teams.length < 2) { 
            this.setState({ errors: { Select: "Please select 2 or more teams to load" }});
            return;
        }
        this.setState({
            stepIndex: this.state.stepIndex + 1,
            finished: this.state.stepIndex > 1,
        });
        let rows = [];  

        this.state.Teams.map((team) => {
            let that = this;
            F.database.ref('TrialPlayers/Phase '+this.state.Phase+'/Futsal/Team '+team).once('value').then(snapshot =>{
                const player = snapshot.val();
                snapshot.forEach(function(data) {
                    var p = {
                        id: data.key,
                        FullName: data.val().FullName,
                        Email: data.val().Email,
                        Year: data.val().Year,
                        Team: data.val().TrialTeam,
                        Phase: 1,
                        Skill1: 0,
                        Skill2: 0,
                        Skill3: 0,
                        NextPhase: ['Yes', 'No'][1]
                    }
                    rows.push(p);
                })
                that.setState({row: rows})
            });
        })  

        //this.props.ratePlayers(this.state.Phase,this.state.Teams);
    };

    handlePrev() { 
        if (this.state.stepIndex > 0) { this.setState({stepIndex: this.state.stepIndex - 1}); }
    };

    handleGridRowsUpdated({ fromRow, toRow, updated }) {
        
        let rows = this.state.row.slice();

        for (let i = fromRow; i <= toRow; i++) {
            let rowToUpdate = rows[i];
            let updatedRow = update(rowToUpdate, {$merge: updated});
            rows[i] = updatedRow;
        }
        this.setState({ row: rows });
    }

    onSubmit(e){
       this.props.ratePlayers(this.state.row)
    }

    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return <PhaseOne  
                            Phase={this.state.Phase}
                            Teams={this.state.Teams}
                            TotalTeams={this.state.TotalTeams}
                            onChange={this.onChange}
                            handlePhaseChange={this.handlePhaseChange}
                            errorText={this.state.errors.Select}
                        />;
                    //return <h>Hello</h>
            case 1:
                return <RateTable 
                            Teams={this.state.Teams}
                            Phase={this.state.Phase}
                            row={this.state.row}
                            players={this.props.trial}
                            handleGridRowsUpdated={this.handleGridRowsUpdated}
                       />;
            default:
                return 'You\'re a long way from home sonny jim!';
        }
    }

    render(){
        const {finished, stepIndex} = this.state;
        const contentStyle = {margin: '0 16px'};
        return(
            <div>
                <Stepper activeStep={stepIndex}>
                    <Step><StepLabel>Players To Load</StepLabel></Step>
                    <Step><StepLabel>Rate Players</StepLabel></Step>
                </Stepper>
                <div style={contentStyle}>
                {this.getStepContent(stepIndex)}            
                    <div style={{marginTop: 12}}>
                        <FlatButton
                            label="Back"
                            disabled={stepIndex === 0}
                            onTouchTap={this.handlePrev}
                            style={{marginRight: 12}}
                         />
                        <RaisedButton
                            label={stepIndex === 1 ? 'Submit' : 'Next'}
                            disabled={stepIndex >= 2}
                            primary={true}
                            onTouchTap={stepIndex === 1 ? this.onSubmit : this.handleNext}
                        />
                     </div> 
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        user: state.user,
        register: state.register,
        trial: state.trial
    };
}

function matchDispatchToProps(dispath){
    return bindActionCreators({ratePlayers: ratePlayers}, dispath)
}

export default connect(mapStateToProps, matchDispatchToProps)(RatingPageForm);