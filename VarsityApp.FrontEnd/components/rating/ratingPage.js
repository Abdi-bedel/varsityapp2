﻿import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import MySelect from './selectTeams';

class RatingPage extends React.Component {
    constructor(props){
        super(props);   
    }

    render(){
        return(
            <div>
            <SelectField
                floatingLabelText="Phase"
                value={this.props.Phase}
                onChange={this.props.handlePhaseChange}
                fullWidth={true}
            >
                <MenuItem value={1} primaryText="Phase 1" />
                <MenuItem value={2} primaryText="Phase 2" />
            </SelectField>
            <MySelect 
                value={this.props.Teams}
                onChange={this.props.onChange}
                Totalteams={this.props.TotalTeams}
                errorText={this.props.errorText}
            />
            </div>
        );
    }
}

export default RatingPage;