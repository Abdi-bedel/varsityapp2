﻿import React from "react";
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class SelectYear extends React.Component {
    constructor(props){
        super(props);
        this.menuItems = this.menuItems.bind(this)
    }

    menuItems(values) {
        const teams = [];
        for (let i = 1; i <= this.props.Totalteams; i++) {
           teams.push({key: i,label: "Team "+i, value: i})
        }
        return teams.map((o) => (
          <MenuItem
            key={o.value}
            insetChildren={true}
            checked={this.props.value && this.props.value.includes(o.value)}
            value={o.value}
            primaryText={o.label}
          />
         ));
    }

    render() {
        const {values} = this.props.value;
        return (
            <SelectField
                multiple={true}
                hintText="Teams to load"
                value={this.props.value}
                onChange={this.props.onChange}
                fullWidth={true}                                errorText={this.props.errorText}
                errorText={this.props.errorText}
            >
                {this.menuItems(this.props.value)}
            </SelectField>
        );
    }
}

export default SelectYear;