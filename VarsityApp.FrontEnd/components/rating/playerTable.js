﻿import React from 'react';
import {connect} from 'react-redux';
const ReactDataGrid = require('react-data-grid');
import {bindActionCreators} from 'redux'

const Example = React.createClass({
    getInitialState() {
        this.createRows();
        this._columns = [
          { key: 'FullName', name: 'Name' },
          { key: 'Year', name: 'Year' },
          { key: 'Skill1', name: 'Skill 1' },
          { key: 'Skill2', name: 'Skill 2' },
          { key: 'Skill3', name: 'Skill 3' }
        ];

        return null;
    },

    createRows() {
        let rows = [];
        const players = this.props.register;
        console.log(players)
        for (let i = 1; i < 10; i++) {
            rows.push({
                id: i,
                title: 'Title ' + i,
                count: i * 1000
            });
        }
        this._rows = rows;
    },

    rowGetter(i) {
        return this._rows[i];
    },

    render() {
        return  (
          <ReactDataGrid
            columns={this._columns}
            rowGetter={this.rowGetter}
            rowsCount={this._rows.length}
            minHeight={500} />
            );
        }
});

function mapStateToProps(state){
    return {
      register: state.register  
    };
}


export default connect(mapStateToProps, null)(Example);