﻿import React from 'react';
import { Card } from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import initialState from '../../actions/initialState.js';
import TweetListItem from './tweetListItem';

const Tweet = ({tweets}) => {
    return (
        <Card>
            <List>
                {tweets.map((tweet, index, value) =>
                    <TweetListItem 
                        key={index}
                        content={tweet.content}
                        sport={tweet.sport}
                        fullname={tweet.fullname}
                    />
                )}
            </List>
        </Card>  
     );
}

export default Tweet;