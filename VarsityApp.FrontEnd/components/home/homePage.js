﻿import React from 'react';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import { Row, Col } from 'react-materialize';
import TweetList from './tweetList.js';
import TweetForm from './tweetForm.js';
import {addTweet} from '../../actions/tweetActions.js';
import * as F from '../../firebase.js';

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            key: '',
            fullname: '',
            sport: '',
            role: '',
            content: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    onChange(e){
        this.setState({content: e.target.value});
        this.setState ({
            key: '',
            fullname: this.props.user.fullname,
            sport: this.props.user.sport,
            role: this.props.user.role
        })
    }

    onClick(e){
        e.preventDefault();  
        this.props.addTweet(this.state);
        this.setState({ content:'' })
    }

    render() {        
        return (
            <Row>
                <Col s={5}>
                </Col>
                <Col s={7}>
                {this.props.user.loggedIn ? <TweetForm onChange={this.onChange} onClick={this.onClick} value={this.state.content} /> : ''}
                    <br />
                    <TweetList tweets={this.props.tweets} />
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tweets: state.tweet,
        user: state.user.user
    };
}

function matchDispatchToProps(dispath){
    return bindActionCreators({addTweet: addTweet}, dispath)
}

export default connect(mapStateToProps, matchDispatchToProps)(Home);