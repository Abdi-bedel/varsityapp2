﻿import React from 'react';
import {ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';

class TweetListItem extends React.Component {
    render() {
        return (
            <ListItem
                leftIcon={<Avatar />}
                secondaryText={this.props.fullname + " @ " + this.props.sport}               
            >
                {this.props.content}
            </ListItem>
        );
    }
}

export default TweetListItem;