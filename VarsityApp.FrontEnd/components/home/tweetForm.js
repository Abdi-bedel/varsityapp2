﻿import React from 'react';
import initialState from '../../actions/initialState.js';
import MyInput from '../common/inputFields.js';
import MyButton from '../common/button.js';

class TweetForm extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
            <MyInput
                label="Let Them Know"
                    name="newTweet"
                    type="text"
                    onChange={this.props.onChange}
                    value={this.props.value}
                />
            <MyButton label="Submit" onClick={this.props.onClick} />
            </div>
        );
    }
}

export default TweetForm;