﻿import {combineReducers} from 'redux';
import tweetReducer from './tweetReducer.js';
import registerReducer from './registerReducer.js';
import trialReducer from './trialReducer.js';
import userReducer from './userReducer.js';

export default combineReducers({
    trial: trialReducer,
    tweet: tweetReducer,
    register: registerReducer,
    user: userReducer
});