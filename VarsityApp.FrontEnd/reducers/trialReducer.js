﻿import * as C from '../constants.js';

const players = {
    Phase: 1,
    Teams: [],
    TotalTeams: 3
}

export default (state = {players}, action) => {
    switch(action.type) {
        case C.LOAD_RATEPLAYERS:
            const newList = action.players;
            return newList;
        case C.LOAD_RATEPLAYERS_FAILURE:
            return;
        case C.LOAD_RATEPLAYERS_SUCCESS:
            return;
        case C.SUBMIT_RATEDPLAYERS:
            return;
        case C.SUBMIT_RATEDPLAYERS_SUCCESS:
            return;
        case C.SUBMIT_RATEDPLAYERS_FAILURE:
            return;
    }   

    return state;
}