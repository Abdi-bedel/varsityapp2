﻿import * as C from '../constants.js';

const user = {
        fullname: '',
        role: '',
        sport: '',
        loggedIn: false,
        error: ''
}, 
pages = {
        signup: true,
        ratingplayer: false
}

export default (state = {user,pages}, action) => {
    switch(action.type) {       
        case C.LOGIN_USER_SUCCESS:
            return Object.assign({}, state, {
                    user: {
                        fullname: action.user.fullname,
                        role: action.user.role,
                        sport: action.user.sport,
                        loggedIn: true
                    }
            });
        case C.LOGIN_USER_SET_PAGES:
            console.log(action.pages)
            return Object.assign({}, state, {
                pages: {
                    signup: true,
                    ratingplayer: action.pages.ratingplayer
                }
            });
        case C.LOGOUT_USER:
            state = {user,pages}
            return state;
        case C.LOGOUT_USER_SUCCESS:
            return;
        case C.LOGOUT_USER_FAILURE:
            return state;
    }   

    return state;
}