﻿import * as C from '../constants.js';
import update from 'react-addons-update';

const tweets = []

export default (state = [], action) => {
    switch(action.type) {
        case C.ADD_TWEET:
            const newList = [ Object.assign({}, action.tweet ), ...state ];
            return newList;
        case C.LOAD_TWEETS_SUCCESS:
            const tweetList = [];
            Object.keys(action.tweets).map(function(key, value) {
                tweetList.push( action.tweets[key] )
            }) 
            return tweetList;
        case C.LOAD_TWEETS_FAILURE:
            return state;
    }   
    return state;
}