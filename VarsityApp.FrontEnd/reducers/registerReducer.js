﻿import initialState from '../actions/initialState.js';
import * as actions from '../constants.js';
import * as F from '../firebase.js';

export default (state = [], action) => {
    switch(action.type) {
        case actions.REGISTER_PLAYER_SUCCESS: 
            return action.response;
        case actions.REGISTER_PLAYER_FAILURE:
            console.log(action.response)
            return action.response;
    }   
    return state;
}   
