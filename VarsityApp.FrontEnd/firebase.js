﻿import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBzC1Vv1frwiCVRVdfFh-3NKbJmVA9Ft-I",
    authDomain: "varsityapp-5871e.firebaseapp.com",
    databaseURL: "https://varsityapp-5871e.firebaseio.com",
    storageBucket: "varsityapp-5871e.appspot.com",
    messagingSenderId: "248680747924"
};

firebase.initializeApp(config);

export const database = firebase.database();
export const auth = firebase.auth();
export const user = firebase.auth().currentUser;

export const storageKey = 'KEY_FOR_LOCAL_STORAGE';

export const isAuthenticated = () => {
    return !!auth.currentUser || !!localStorage.getItem(storageKey);
}