﻿import * as actions from '../constants.js';
import axios from 'axios';
import * as F from '../firebase.js';

export const registerPlayer = (player) => {
    return function(dispatch) {
        axios.post('http://localhost:62696/api/addplayer', player).then(response => {
            const newPlayer = response.data;
            F.database.ref('TrialTeamInfo/Phase '+newPlayer.Sport+'/Phase '+newPlayer.Phase).set(newPlayer.SportInfo);          
            const newTrialPlayer = {
                FullName: newPlayer.FullName,
                Email: newPlayer.Email,
                Position: newPlayer.Position,
                Phone: newPlayer.Phone,
                Phase: newPlayer.Phase,
                Year: newPlayer.Year,
                Sport: newPlayer.Sport,
                TrialTeam: newPlayer.TrialTeam,
                Skill1: 0,
                Skill2: 0,
                Skill3: 0
            }
            F.database.ref('TrialPlayers/Phase 1/'+newPlayer.Sport+'/Team '+newPlayer.TrialTeam).push(newTrialPlayer);
            dispatch({type: actions.REGISTER_PLAYER_SUCCESS, response})
        }).catch(error => {
            console.log("Error", error);
            dispatch({type: actions.REGISTER_PLAYER_FAILURE, error});
        })
    }
}