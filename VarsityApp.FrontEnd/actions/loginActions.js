﻿import * as C from '../constants.js';
import * as F from '../firebase.js';

export const LoginUser = (email, pass) => {
    return function(dispatch) {        
        F.auth.signInWithEmailAndPassword(email, pass).then(user => {
        }).catch(error => {
            const errormessage = error.message;
            dispatch({type: C.LOGIN_USER_FAILURE, errormessage})
        })    
    }
}

export const stayLoggedIn = () => {
    return function(dispatch) {        
        F.auth.onAuthStateChanged(function(user) {
            if (user) {
                // User is signed in.
                dispatch(loginSuccess(user));
            } else {
                // No user is signed in.
                dispatch({type: C.LOGOUT_USER})
            }
        });
    }
}

export const logOut = () => {
    return function(dispatch) {
        F.auth.signOut();
    }
}

export const newUser = (Email, Pass, Name, Sport, Role) => {
    return function(dispatch) {
        F.auth.createUserWithEmailAndPassword(Email, Pass)
            .then(function(newUser) {
                const user = {
                    fullname: Name,
                    role: Role,
                    sport: Sport
                }
                console.log(newUser)
                F.database.ref('User/' + newUser.uid).set(user);
                F.database.ref('Utills/Pages/' + user.role).once('value').then(snapshot =>{
                    const pages = snapshot.val();
                    dispatch({type: C.LOGIN_USER_SET_PAGES, pages})
                });     
            }).catch(function(error) {
                dispatch({type: C.LOGOUT_USER_FAILURE, error})
            })
    }
}

export const loginSuccess = (user) => {
    return function(dispatch) {
        F.database.ref('User/' + user.uid).once('value').then(snapshot => {
            const user = snapshot.val();
            F.database.ref('Utills/Pages/' + user.role).once('value').then(snapshot =>{
                const pages = snapshot.val();
                dispatch({type: C.LOGIN_USER_SET_PAGES, pages})
            });     
            dispatch({type: C.LOGIN_USER_SUCCESS, user})
        }).catch(error => {
            console.log("Error ",error)
        })    
    }
}