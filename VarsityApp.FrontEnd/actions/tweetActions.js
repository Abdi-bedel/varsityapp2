﻿import * as C from '../constants.js';
import * as F from '../firebase.js';
import initialState from './initialState.js';

export const addTweet = (tweet) => {
    return function(dispatch) {
        //dispatch({type: C.LOAD_TWEETS_SUCCESS, initialState})
        F.database.ref('Tweets').push(tweet)
        dispatch({ type: C.ADD_TWEET, tweet })        
    }
}

export function loadTweet() {
    return function(dispatch) {
        //dispatch({type: C.LOAD_TWEETS_SUCCESS, initialState})
        F.database.ref('Tweets').once('value').then(snapshot => {
            const tweets = snapshot.val();
            dispatch({type: C.LOAD_TWEETS_SUCCESS, tweets})
        }).catch(error => {
            dispatch({type: C.LOAD_TWEETS_FAILURE})   
        })
    }
}
