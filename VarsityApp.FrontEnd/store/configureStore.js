﻿import {createStore, applyMiddleware} from 'redux';
import allReducers from '../reducers/combineReducers.js';
import initialState from '../actions/initialState.js';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {loadTweet} from '../actions/tweetActions.js';
import {stayLoggedIn} from '../actions/loginActions.js';

const store = createStore(
    allReducers,
    applyMiddleware(thunk, logger())
 );
store.dispatch(stayLoggedIn())
store.dispatch(loadTweet())

export default store;

