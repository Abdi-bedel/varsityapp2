﻿import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import App from './components/app.js';
import store from './store/configureStore.js';
import {Provider} from 'react-redux';
import allReducers from './reducers/combineReducers.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from './lib/injectTapEventPlugin.js';

injectTapEventPlugin();

class Main extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <MuiThemeProvider>
                    <App />
                </MuiThemeProvider>
            </Provider>
        );
    }
};

ReactDOM.render(
    <Main />,
    document.getElementById('app')
);